package pt.ipp.isep.dei.esoft.gpsd.model;
import java.util.List;

public interface ServicoExterno {

	public List<AtuaEm> obtemAtuacao(Object aCodPostal_base, Object aFloat_raio);
}